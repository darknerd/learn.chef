# **Tomcat 8 Server Implementation**

This is an implementation of Tomcat 8.

## **Notes**

Updates made to reflect current versions as of 2018年05月20日:

* Cent OS 7.4
* Tomcat 8.0.52

## **References**

Original material inspired by [Joy of Automating Episode 1](https://youtu.be/FOYc_SGWE-0) that uses Chef 12 and ServerSpec.
