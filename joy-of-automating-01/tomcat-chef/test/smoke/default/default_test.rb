# # encoding: utf-8

# Inspec test for recipe tomcat::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

##################### IMPLEMENTATION TESTS #####################
# These tests the specific implmentation details, which is not necessary.
# They are here for demonstration purposes
##########################################
describe package('java-1.7.0-openjdk-devel') do
  it { should be_installed }
end

describe group('tomcat') do
  it { should exist }
end

describe user('tomcat') do
  it { should exist }
  its('group') { should eq 'tomcat' }
  its('home') { should eq '/opt/tomcat' }
  its('shell') { should eq '/bin/nologin' }
end

describe directory('/opt/tomcat') do
  it { should exist }
  its('group') { should eq 'tomcat' }
  its('owner') { should eq 'root' }
end

%w(webapps work temp logs).each do |dir|
  describe directory("/opt/tomcat/#{dir}") do
    its('mode') { should cmp '0755' }
    it { should exist }
    its('group') { should eq 'tomcat' }
    its('owner') { should eq 'tomcat' }
  end
end

describe service('tomcat') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

describe port 8080 do
  it { should be_listening }
end

##################### ACTUAL SMOKE TESTS #####################
describe http('http://localhost:8080/', enable_remote_worker: true) do
  its('status') { should cmp 200 }
  its('headers.Content-Type') { should cmp 'text/html;charset=UTF-8' }
  its('headers.Server') { should cmp 'Apache-Coyote/1.1' }
end

describe http('127.0.0.1:8080/manager/html',
              auth: {user: 'admin', pass: 'password'},
              enable_remote_worker: true) do
  its('status') { should cmp 200 }
  its('headers.Content-Type') { should cmp 'text/html;charset=UTF-8' }
  its('headers.Server') { should cmp 'Apache-Coyote/1.1' }
end

describe http('127.0.0.1:8080/host-manager/html',
              auth: {user: 'admin', pass: 'password'},
              enable_remote_worker: true) do
  its('status') { should cmp 200 }
  its('headers.Content-Type') { should cmp 'text/html;charset=UTF-8' }
  its('headers.Server') { should cmp 'Apache-Coyote/1.1' }
end
