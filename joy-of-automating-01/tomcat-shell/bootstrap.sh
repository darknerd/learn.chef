#!/usr/bin/env bash
set -e

##################### VARIABLES #####################
APP_DIR="/opt/tomcat"
VERSION="8.0.52"
PACKAGE="apache-tomcat-${VERSION}.tar.gz"
PREFIX="http://apache.mirrors.ionfish.org/tomcat/tomcat-8"
PKG_URL="${PREFIX}/v${VERSION}/bin/${PACKAGE}"


##################### PREREQS #####################
yum install -y java-1.7.0-openjdk-devel
getent group tomcat || groupadd tomcat
id -u tomcat || useradd -M -s /bin/nologin -g tomcat -d /opt/tomcat tomcat

##################### INSTALL #####################

cd ~
wget ${PKG_URL}

[[ -d ${APP_DIR} ]] || mkdir ${APP_DIR}
tar xvf apache-tomcat-8*tar.gz -C ${APP_DIR} --strip-components=1

cd ${APP_DIR}
chgrp -R tomcat ${APP_DIR}
chmod -R g+r conf
chmod g+x conf
chown -R tomcat webapps/ work/ temp/ logs/


##################### SERVICE #####################
cat <<-UNIT > /etc/systemd/system/tomcat.service
# Systemd unit file for tomcat
[Unit]
Description=Apache Tomcat Web Application Container
After=syslog.target network.target

[Service]
Type=forking

Environment=JAVA_HOME=/usr/lib/jvm/jre
Environment=CATALINA_PID=${APP_DIR}/temp/tomcat.pid
Environment=CATALINA_HOME=${APP_DIR}
Environment=CATALINA_BASE=${APP_DIR}
Environment='CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC'
Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'

ExecStart=${APP_DIR}/bin/startup.sh
ExecStop=/bin/kill -15 \$MAINPID

User=tomcat
Group=tomcat
UMask=0007
RestartSec=10
Restart=always

[Install]
WantedBy=multi-user.target
UNIT

systemctl daemon-reload
systemctl start tomcat
systemctl enable tomcat

##################### MANAGEMENT INTERFACE #####################
sed -i '/<\/tomcat-users>/ i\  <user username="admin" password="password" roles="manager-gui,admin-gui"\/>' ${APP_DIR}/conf/tomcat-users.xml
cat <<-'MANAGER_CONTEXT' > ${APP_DIR}/webapps/manager/META-INF/context.xml
<?xml version="1.0" encoding="UTF-8"?>
<Context antiResourceLocking="false" privileged="true" >
  <Valve className="org.apache.catalina.valves.RemoteAddrValve"
         allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" />
  <Manager sessionAttributeValueClassNameFilter="java\.lang\.(?:Boolean|Integer|Long|Number|String)|org\.apache\.catalina\.filters\.CsrfPreventionFilter\$LruCache(?:\$1)?|java\.util\.(?:Linked)?HashMap"/>
</Context>
MANAGER_CONTEXT
cat <<-'HOSTMGR_CONTEXT' > ${APP_DIR}/webapps/host-manager/META-INF/context.xml
<?xml version="1.0" encoding="UTF-8"?>
<Context antiResourceLocking="false" privileged="true" >
  <Valve className="org.apache.catalina.valves.RemoteAddrValve"
         allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" />
  <Manager sessionAttributeValueClassNameFilter="java\.lang\.(?:Boolean|Integer|Long|Number|String)|org\.apache\.catalina\.filters\.CsrfPreventionFilter\$LruCache(?:\$1)?|java\.util\.(?:Linked)?HashMap"/>
</Context>
HOSTMGR_CONTEXT

systemctl restart tomcat
