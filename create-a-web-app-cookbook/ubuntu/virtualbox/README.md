# **Create "The Customers" Web App Cookbook Tutorial**

## **Prerequisites**

* [Virtualbox](https://www.virtualbox.org/)
* [Vagrant](https://www.vagrantup.com/)

## **Start Environment**

```bash
vagrant up
vagrant ssh
curl "https://omnitruck.chef.io/install.sh" | sudo bash -s -- -P 'chefdk' -c 'stable' -v '2.5.3'
# Do your code edits in /vagrant/cookbooks as needed
```

## **Integration Tests**

```bash
kitchen converge
kitchen verify
```

## **Reference**

* https://learn.chef.io/modules/create-a-web-app-cookbook/ubuntu/virtualbox/create-the-customers-web-app#/