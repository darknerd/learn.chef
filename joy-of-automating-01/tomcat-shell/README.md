# **Tomcat 8 Server Implementation: Shell**

This is an implementation of Tomcat 8 using [Digital Ocean Tomcat 8 on Cent OS 7 guide](https://www.digitalocean.com/community/tutorials/how-to-install-apache-tomcat-8-on-centos-7) and corresponding InSpec tests.
